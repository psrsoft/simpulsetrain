//  Copyright (C) 2015 George Hobbs

/*                                                                                                                                                     
 *    This file is part of simpulsetrain.                                                                                                                      
 *                                                                                                                                                      
 *    SimPulseTrain is free software: you can redistribute it and/or modify                                                                                    
 *    it under the terms of the GNU General Public License as published by                                                                              
 *    the Free Software Foundation, either version 3 of the License, or                                                                                 
 *    (at your option) any later version.                                                                                                               
 *    SimPulseTrain is distributed in the hope that it will be useful,                                                                                         
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                    
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                     
 *    GNU General Public License for more details.                                                                                                      
 *    You should have received a copy of the GNU General Public License                                                                                 
 *    along with simPulseTrain.  If not, see <http://www.gnu.org/licenses/>.                                                                                   
 */



// Compile with
// gcc -lm -o simPulseTrain simPulseTrain.c T2toolkit.c
//
// Program to simulate a "simple" pulse train, but with moderately realistic frequency-dependent properties
//
// The user requests a certain observation time, pulse period, intrinsic pulse width ... and number of observed frequency channels
// 
// The software calculates the pulse signal in a much larger number of frequency channels than requested by the user in order
// to account for dispersion smearing and scattering effects.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "T2toolkit.h"
#include "header.h"

double calcPulseAmp(double pulseFlux,double pulseFluxFreq,double pulseSI,double period,double tsamp,double width,double freq);
void send_string(char *string,FILE *output); 
void send_int(char *name, int integer,FILE *output); 
void send_double (char *name, double double_precision,FILE *output); 
void writeFilterbankFile(char *fname,int nsblk,double f1,double chanbw,int nc,double stime,float *outSamp,unsigned long int nSamp);
double calcDriftScale(double telDiameter,double declination,double freq,double timeVal);

int main(int argc,char *argv[])
{
  //
  // User parameters
  //
  int    nWhiteRealisation = 1; // Number of realisations of white noise
  int    nFluxDensityScalings = 1; // Number of different flux density scalings
  float  fluxScalings[500];
  //  double pulseFlux = 20e-6; // Pulse flux density in Jy at specified frequency
  double pulseFlux = 150e-6; // Pulse flux density in Jy at specified frequency
  double pulseFluxFreq = 1400.0; // Pulse flux measurement frequency (MHz)
  double pulseSI = -1.4; // Pulse spectral index

  double period=0.1;      // Period in seconds
  double width = 0.02;
  int    nChanRequested = 8;
  double tsamp = 50e-6; // Sampling rate (sec)
  double obsLenReq = 50; // Seconds
  double dm = 20; // cm^-3pc
  double f_low = 900; // Frequency at low part of band (MHz)
  double f_high = 1400; // Frequency at top part of the band (MHz)

  int    nPolSum = 2;   // Number of polarisation that are summed
  double gain    = 16;  // Gain in K/Jy
  double trcvr   = 35;  // T_receiver in K
  double tsky0   = 30;   // Sky temperature at specified frequency (K)
  double tskyFreq = 600; // Frequency (in MHz) for the tsky measurement
  char   inputParamFile[128]="NULL";
  int    outputMultiChannels = 1;
  int    driftScan = 0;  // Are we doing a drift scan?
  double telDiameter = 300; // Telescope diameter
  double declination = 45;  // Source declination

  int    outWeightedFile = 0;

  // Simulation parameters
  //
  int    nChanSim;
  unsigned long int    nSamp;
  int    nsblk = 1024;  // Number of samples per block
  float *samp,*signalVals,*noiseAmps;
  float *outSamp,*dispersed,*wdispersed;
  double weighting;
  unsigned long int i;
  int   j,k;
  int    chan,chanR;
  double time,phase;
  double dispersionDelay;
  int    intDelay;
  double freq;
  double timeVal;
  double chanbw;
  double reqChanBW;
  double fcentral;
  double pulseAmp;
  double signal,noise;
  double noiseLevel;
  FILE *fout;
  char tempStr[1024];
  long  seed = TKsetSeed();
  double tsys,tsky;
  int   channelDivisor = 10;
  double driftScale;
  double nullProb = 0;
  float *pulseFluxVariation; 
  long int nPulses,nPulse;
  float ranValue;

  fluxScalings[0] = pulseFlux;

  // Read inputs
  for (i=0;i<argc;i++)
    {
      if (strcmp(argv[i],"-p")==0)
	strcpy(inputParamFile,argv[++i]);
      else if (strcmp(argv[i],"-nofil")==0)
	outputMultiChannels = 0;
    }

  // Read the input parameters
  if (strcmp(inputParamFile,"NULL")==0)
    printf("Using default parameters\n");
  else
    {
      FILE *fin;
      char str[1024];
      if (!(fin = fopen(inputParamFile,"r")))
	{
	  printf("Unable to open file: %s\n",inputParamFile);
	  exit(1);
	}
      while (!feof(fin))
	{
	  if (fscanf(fin,"%s",str)==1)
	    {
	      if (strcasecmp(str,"period")==0)
		fscanf(fin,"%lf",&period);
	      else if (strcasecmp(str,"width")==0)
		fscanf(fin,"%lf",&width);
	      else if (strcasecmp(str,"nchan")==0)
		fscanf(fin,"%d",&nChanRequested);
	      else if (strcasecmp(str,"nullProb")==0)
		fscanf(fin,"%lf",&nullProb);
	      else if (strcasecmp(str,"tsamp")==0)
		fscanf(fin,"%lf",&tsamp);
	      else if (strcasecmp(str,"obslen")==0)
		fscanf(fin,"%lf",&obsLenReq);
	      else if (strcasecmp(str,"dm")==0)
		fscanf(fin,"%lf",&dm);
	      else if (strcasecmp(str,"f_low")==0)
		fscanf(fin,"%lf",&f_low);
	      else if (strcasecmp(str,"f_high")==0)
		fscanf(fin,"%lf",&f_high);
	      else if (strcasecmp(str,"npolsum")==0)
		fscanf(fin,"%d",&nPolSum);
	      else if (strcasecmp(str,"gain")==0)
		fscanf(fin,"%lf",&gain);
	      else if (strcasecmp(str,"trcvr")==0)
		fscanf(fin,"%lf",&trcvr);
	      else if (strcasecmp(str,"tsky0")==0)
		fscanf(fin,"%lf",&tsky0);
	      else if (strcasecmp(str,"tskyfreq")==0)
		fscanf(fin,"%lf",&tskyFreq);
	      else if (strcasecmp(str,"tskyfreq")==0)
		fscanf(fin,"%lf",&tskyFreq);
	      else if (strcasecmp(str,"pulseFlux")==0)
		{
		  fscanf(fin,"%lf",&pulseFlux);
		  fluxScalings[0] = pulseFlux;
		}
	      else if (strcasecmp(str,"pulseFluxFreq")==0)
		fscanf(fin,"%lf",&pulseFluxFreq);
	      else if (strcasecmp(str,"pulseSI")==0)
		fscanf(fin,"%lf",&pulseSI);
	      else if (strcasecmp(str,"nFluxScalings")==0)
		fscanf(fin,"%d",&nFluxDensityScalings);
	      else if (strcasecmp(str,"fluxScalings")==0)
		{
		  for (i=0;i<nFluxDensityScalings;i++)
		    fscanf(fin,"%f",&fluxScalings[i]);
		}	
	      else if (strcasecmp(str,"nWhiteRealisations")==0)
		fscanf(fin,"%d",&nWhiteRealisation);
	      else if (strcasecmp(str,"channelDivisor")==0)
		fscanf(fin,"%d",&channelDivisor);
	      else if (strcasecmp(str,"driftScan")==0)
		fscanf(fin,"%d",&driftScan);
	      else if (strcasecmp(str,"telDiameter")==0)
		fscanf(fin,"%lf",&telDiameter);
	      else if (strcasecmp(str,"declination")==0)
		fscanf(fin,"%lf",&declination);
	      else if (strcasecmp(str,"weightedFile")==0)
		fscanf(fin,"%d",&outWeightedFile);
	    }
	}
    }
  // Convert the width from FWHM (as defined by the user) to the width defined by a Gaussian function
  width = width/2.3;


  // Calculate nSamp 
  // This should be close to the requested obsLen, but should fit into an integral number of nsblk
  // (LATER SHOULD ALSO ALLOW A REQUEST OF A 2^N NUMBER OF POINTS -- NOT DONE)
  nSamp = (unsigned long int)((obsLenReq/tsamp)/nsblk+0.5)*nsblk;
  printf("Requested observing time = %g, simulated observing time = %g seconds\n",obsLenReq,nSamp*tsamp);
  
  // Determine the number of channels that should be simulated
  nChanSim = nChanRequested*channelDivisor; // MUST THINK HOW TO DO THIS PROPERLY

  // Allocate enough memory for this number of channels
  // Store the data here as <all data for channel 1><all data for channel 2> ... <all data for channel n>
  if (!(samp = (float *)malloc(sizeof(float)*nChanSim*nSamp)))
    {
      printf("Unable to allocate sufficient memory for the samples\n");
      exit(1);
    }
  if (!(signalVals = (float *)malloc(sizeof(float)*nChanSim*nSamp)))
    {
      printf("Unable to allocate sufficient memory for the samples\n");
      exit(1);
    }
  //  if (!(noiseAmps = (float *)malloc(sizeof(float)*nChanSim*nSamp)))
  //    {
  //      printf("Unable to allocate sufficient memory for the samples\n");
  //      exit(1);
  //    }
  if (!(dispersed = (float *)malloc(sizeof(float)*nSamp)))
    {
      printf("Unable to allocate sufficient memory for the samples\n");
      exit(1);
    }
  if (!(wdispersed = (float *)malloc(sizeof(float)*nSamp)))
    {
      printf("Unable to allocate sufficient memory for the samples\n");
      exit(1);
    }

  // Calculate the channel bandwidth given the low and high frequencies
  chanbw = (f_high-f_low)/nChanSim;
  reqChanBW = (f_high-f_low)/nChanRequested;
  fcentral = (f_high+f_low)/2.0;

  // Set up the pulse flux variations
  nPulses  = (int)((nSamp*tsamp)/period); // Should account for dispersion delay
  pulseFluxVariation = (float *)malloc(sizeof(float)*nPulses); 
  for (i=0;i<nPulses;i++)
    {
      ranValue = TKranDev(&seed);
      printf("ranVal = %g\n",ranValue);
      if (ranValue >= nullProb)
      	pulseFluxVariation[i] = 1;
      else
      	pulseFluxVariation[i] = 0;
      //      if (i<5) pulseFluxVariation[i] = 0.5;
      //      else pulseFluxVariation[i] = 1;

    }
  printf("Number of pulses is %d\n",nPulses);

  for (chan=0;chan < nChanSim;chan ++)
    {
      // Calculate frequency of this channel in Hz
      freq = (f_low + chan*chanbw + chanbw/2.0)*1e6;
      
      // Calculate dispersion delay
      // MUST CHECK VERY CAREFULLY THAT THIS DELAY IS NOT TOO LARGE FOR THE FREQUENCY CHANNELS
      dispersionDelay = 4.15e3*dm*(1.0/(pow(freq/1.0e6,2)-1.0/(pow(fcentral,2)))); // Note different units for freq and fcentral

      // Calculate the pulse amplitude in this frequency channel
      // Assume a flux of 1 and then scale later
      pulseAmp = calcPulseAmp(1,pulseFluxFreq*1e6,pulseSI,period,tsamp,width,freq);

      printf("Processing channel: %d, dispersion delay = %g\n",chan,dispersionDelay);      
      
      for (i=0;i<nSamp;i++)
	{
	  time = i*tsamp;	  
	  // Calculate pulse phase accounting for dispersion
	  //	  phase = (time-dispersionDelay)/period - ((int)((time-dispersionDelay)/period+0.5));
	  phase = (time-dispersionDelay)/period - ((int)((time-dispersionDelay)/period+0.5));
	  nPulse = ((int)((time-dispersionDelay)/period));
	  signalVals[i+chan*nSamp] = pulseFluxVariation[nPulse]*pulseAmp*exp(-pow(phase,2)/(2*pow(width,2)));
	  //	  noiseAmps[i+chan*nSamp] = noiseLevel;
	}
    }
  // Allocate enough memory for this number of channels
  // Store the data here as <all data for channel 1><all data for channel 2> ... <all data for channel n>
  if (!(outSamp = (float *)malloc(sizeof(float)*nChanRequested*nSamp)))
    {
      printf("Unable to allocate sufficient memory for the samples\n");
      exit(1);
    }

  for (k=0;k<nFluxDensityScalings;k++)
    {
      for (j=0;j<nWhiteRealisation;j++)
	{
	  printf("Summing simulated frequency channels flux = %d/%d, white = %d/%d\n",k,nFluxDensityScalings,j,nWhiteRealisation);
		  
	  // Now sum over the divided channels to produce the data set requested by the user
	  for (chanR=0;chanR < nChanRequested;chanR++)
	    {
	      printf("Processing channel %d\n",chanR);
	      //	  sprintf(tempStr,"pulses_scrunched_chan%d",chanR);
	      //	  fout = fopen(tempStr,"w");
	      for (i=0;i<nSamp;i++)
		{
		  for (chan=0;chan<channelDivisor;chan++)
		    {
		      // Scale by gain variations caused by drift scan
		      driftScale = 1;
		      if (driftScan == 1)
			{
			  freq = (f_low + chanR*reqChanBW + chan*chanbw + chanbw/2.0)*1e6; 
			  timeVal = i*tsamp-(nSamp*tsamp/2.0); // Time from centre of observation
			  driftScale = calcDriftScale(telDiameter,declination,freq,timeVal);
			}

		      if (chan==0)
			outSamp[i+(unsigned long int)(chanR*nSamp)] = signalVals[i+(chanR*channelDivisor+chan)*nSamp]*fluxScalings[k]*driftScale;
		      else
			outSamp[i+(unsigned long int)(chanR*nSamp)] += signalVals[i+(chanR*channelDivisor+chan)*nSamp]*fluxScalings[k]*driftScale;
		    }
		  outSamp[i+chanR*nSamp]/=(double)channelDivisor;

		  // Add noise
		  freq = (f_low + chanR*reqChanBW + reqChanBW/2.0)*1e6; // Should probably put this at the centre of the band **
		  // Determine the system noise temperature
		  //
		  tsky  = tsky0*pow(freq/(tskyFreq*1e6),-2.6);
		  tsys = tsky + trcvr;
		  // Determine the radiometer noise level (in Jy)
		  noiseLevel = tsys/gain/sqrt(nPolSum*reqChanBW*1e6*tsamp);

		  outSamp[i+chanR*nSamp] += noiseLevel*TKgaussDev(&seed);
		  //	      fprintf(fout,"%ld %g\n",i,outSamp[i+chanR*nSamp]);
		
		}
	      printf("Finsihed channel %d\n",chanR);
	      //	  fclose(fout);
	    }
	  if (outputMultiChannels == 1)
	    {
	      printf("Outputing filterbank files\n");

	      sprintf(tempStr,"output_white%d_flux%d.fil",j,k);
	      // CHCEK THE REQCHANBW HERE
	      // The frequency should be the "centre frequency of the first channel (in MHz)
	      //
	      writeFilterbankFile(tempStr,nsblk,f_low+reqChanBW/2.0,reqChanBW,nChanRequested,tsamp,outSamp,nSamp);
	      printf("Done\n");
	    }
	  // Create a dedispersed and frequency-scrunched filterbank file
	  for (i=0;i<nSamp;i++)
	    {
	      dispersed[i] = 0.0;
	      wdispersed[i] = 0.0;
	      for (chan=0;chan<nChanRequested;chan++)
		{
		  
		  freq = (f_low + chan*(f_high-f_low)/nChanRequested + (f_high-f_low)/nChanRequested/2.0)*1e6;
		  
		  // Calculate dispersion delay
		  dispersionDelay = 4.15e3*dm*(1.0/(pow(freq/1.0e6,2)-1.0/(pow(fcentral,2)))); // Note different units for freq and fcentral

		  //		  dispersionDelay = 4.15e3*dm/pow(freq/1.0e6,2);
		  intDelay = (int)(dispersionDelay/tsamp+0.5);
		  if (i+intDelay >= nSamp)
		    dispersed[i] = 0; // What should we actually do about this?
		  else
		    dispersed[i]+=outSamp[i+intDelay+chan*nSamp];

		  if (outWeightedFile == 1)
		    {
		      if (i+intDelay >= nSamp)
			wdispersed[i] = 0; // DO SOMETHING HERE
		      else
			{
			  freq = (f_low + chan*reqChanBW + reqChanBW/2.0)*1e6; 
			  timeVal = i*tsamp-(nSamp*tsamp/2.0); // Time from centre of observation
			  weighting = calcDriftScale(telDiameter,declination,freq,timeVal);

			  wdispersed[i]+=outSamp[i+intDelay+chan*nSamp]*weighting;
			}
		    }

		}
	      dispersed[i]/=nChanRequested;

	    }
	  printf("outputing scrunched files\n");
	  sprintf(tempStr,"output_white%d_flux%d_dm.tim",j,k);
	  writeFilterbankFile(tempStr,nsblk,f_low,(f_high-f_low),1,tsamp,dispersed,nSamp);
	  if (outWeightedFile == 1)
	    {
	      sprintf(tempStr,"output_white%d_flux%d_dm_w1.tim",j,k);
	      writeFilterbankFile(tempStr,nsblk,f_low,(f_high-f_low),1,tsamp,wdispersed,nSamp);
	    }
	  printf("done\n");
	  
	}
    }
  // Free the memory
  free(samp);
  free(outSamp);
  free(signalVals);
  //  free(noiseAmps);
  free(dispersed);
  free(pulseFluxVariation);
}

// Write a SIGPROC filterbank file with the data 
void writeFilterbankFile(char *fname,int nsblk,double f1,double chanbw,int nc,double stime,float *outSamp,unsigned long int nSamp)
{
  int arraysize = nsblk*nc; // Assuming 1 channel and 1 IF
  float *block;
  int nsub = nSamp/nsblk;
  int sub;
  int chan;
  FILE *output;
  int i;

  strcpy(source_name,"simulated");
  machine_id = 1;
  telescope_id = 7;
  fch1 = f1;
  foff = chanbw;
  nchans = nc;
  nbits = 32;
  tstart = 53000.0;
  tsamp = stime;
  nifs = 1;

  block = (float *)malloc(sizeof(float)*arraysize);

  output = fopen(fname,"wb");
  // header
  send_string("HEADER_START",output);
  send_string("source_name",output);
  send_string(source_name,output);
  send_int("machine_id",machine_id,output);
  send_int("telescope_id",telescope_id,output);
  if (nc==1) // If nchan = 1 (i.e., dedispersed)
    {
      send_int("data_type",2,output);
      send_double("refdm",0,output); // SHOULD SET THIS PROPERLY
    }
  else
    send_int("data_type",1,output);

  send_double("fch1",fch1,output);
  send_double("foff",foff,output);
  send_int("nchans",nchans,output);
  send_int("nbits",nbits,output);
  send_double("tstart",tstart,output);
  send_double("tsamp",tsamp,output);
  send_int("nifs",nifs,output);
  send_string("HEADER_END",output);

  for (sub=0;sub<nsub;sub++)
    {
      for (i=0;i<nsblk;i++)
	{
	  for (chan=0;chan<nchans;chan++)
	    {	      
	      block[i*nchans+chan] = outSamp[sub*nsblk+i+chan*nSamp];
	    }
	}
      fwrite(block,sizeof(float),arraysize,output);
    }
  fclose(output);
  free(block);

}

// The total area under the pulse amplitude should equal the pulseFlux
// but the pulse may take a non-trivial shape - so let's just directly calculate
// the area under the pulse and then scale it.

double calcPulseAmp(double pulseFlux,double pulseFluxFreq,double pulseSI,double period,double tsamp,double width,double freq)
{
  double amp;
  int nbin = period/tsamp;
  float val[nbin],add;
  float flux;
  int i;
  double phase;

  add=0;
  for (i=0;i<nbin;i++)
    {
      phase = (double)i/(double)nbin;
      val[i] = exp(-pow(phase-0.5,2)/(2*width*width));
      add+=val[i];
    }
  add/=(double)nbin;
  flux = pulseFlux*pow(freq/pulseFluxFreq,pulseSI);
  amp = flux/add;
  return amp;
}

// Useful routines for writing sigproc filterbank files
void send_string(char *string,FILE *output) 
{
  int len;
  len=strlen(string);
  fwrite(&len, sizeof(int), 1, output);
  fwrite(string, sizeof(char), len, output);
  /*fprintf(stderr,"%s\n",string);*/
}

void send_int(char *name, int integer,FILE *output) 
{
  send_string(name,output);
  fwrite(&integer,sizeof(int),1,output);
  /*fprintf(stderr,"%d\n",integer);*/
}

void send_double (char *name, double double_precision,FILE *output) 
{
  send_string(name,output);
  fwrite(&double_precision,sizeof(double),1,output);
  /*fprintf(stderr,"%f\n",double_precision);*/
}

double calcDriftScale(double telDiameter,double declination,double freq,double timeVal)
{
  double driftScale;
  double firstnull;
  double xval;

  firstnull = 1.22*3.0e8/freq/telDiameter;
  xval = timeVal*180.0/(12.0*60.0*60.0)*cos(declination*M_PI/180.0)*M_PI/firstnull*M_PI/180.0;
  if (xval > -1e-6 && xval < 1e-6)
    driftScale = 1;
  else
    driftScale = pow(sin(xval)/xval,2);
  return driftScale;
}
