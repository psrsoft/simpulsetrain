//  Copyright (C) 2015 George Hobbs

/*                                                                                                                                                     
 *    This file is part of simpulsetrain.                                                                                                                      
 *                                                                                                                                                      
 *    SimPulseTrain is free software: you can redistribute it and/or modify                                                                                    
 *    it under the terms of the GNU General Public License as published by                                                                              
 *    the Free Software Foundation, either version 3 of the License, or                                                                                 
 *    (at your option) any later version.                                                                                                               
 *    SimPulseTrain is distributed in the hope that it will be useful,                                                                                         
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                    
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                     
 *    GNU General Public License for more details.                                                                                                      
 *    You should have received a copy of the GNU General Public License                                                                                 
 *    along with simPulseTrain.  If not, see <http://www.gnu.org/licenses/>.                                                                                   
 */


/* global variables describing the data */
char rawdatafile[80], source_name[80];
int machine_id, telescope_id, data_type, nchans, nbits, nifs, scan_number,
  barycentric,pulsarcentric; /* these two added Aug 20, 2004 DRL */
double tstart,mjdobs,tsamp,fch1,foff,refdm,az_start,za_start,src_raj,src_dej;
double gal_l,gal_b,header_tobs,raw_fch1,raw_foff;
int nbeams, ibeam;
char isign;
/* added 20 December 2000    JMC */
double srcl,srcb;
double ast0, lst0;
long wapp_scan_number;
char project[8];
char culprits[24];
double analog_power[2];

/* added frequency table for use with non-contiguous data */
double frequency_table[4096]; /* note limited number of channels */
long int npuls; /* added for binary pulse profile format */


// define the signedness for the 8-bit data type
#define OSIGN 1
#define SIGNED OSIGN < 0
